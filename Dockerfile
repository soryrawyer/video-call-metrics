# We're not relying on any significant packages, so let's use Alpine Linux
# as our base image. This should keep the image size a little smaller
FROM python:3.9-alpine

RUN mkdir -p /app/daily_video_metrics/
WORKDIR /app/daily_video_metrics

RUN pip install pipenv

COPY Pipfile .
COPY Pipfile.lock .

# install these dependencies to the system, not into a virtual environment
RUN pipenv install --system

COPY app /app/daily_video_metrics/app
COPY run.py /app/daily_video_metrics/run.py
COPY init_db.py /app/daily_video_metrics/init_db.py
RUN python init_db.py

# expose port 8000 so we can access the application from outside the container
EXPOSE 8000
CMD ["gunicorn", "--bind", ":8000", "-w", "4", "run:app"]
