"""
an architecture diagram of the video call service
"""

import argparse

from diagrams import Diagram, Cluster, Edge
from diagrams.onprem.client import User
from diagrams.onprem.compute import Server
from diagrams.onprem.network import Nginx
from diagrams.programming.framework import Flask


def main(args):
    """
    create a diagram of our video call service and write it to the given
    filename
    """
    # show=False means we just want to write the diagram, not launch a
    # viewer as part of this script

    encrypted_edge = Edge(color="darkgreen", label="encrypted with Let's Encrypt")

    with Diagram(args.filename, show=False):
        user = User("End user")
        with Cluster("digital ocean"):
            # load balancer, droplet, nginx, flask, SSL
            load_balancer = Server("DO load balancer")

            user >> encrypted_edge >> load_balancer

            with Cluster("application server"):
                nginx = Nginx("NGINX")
                load_balancer >> Edge(label="SSL terminates at load balancer") >> nginx
                flask = Flask()
                nginx >> flask


if __name__ == "__main__":
    parser = argparse.ArgumentParser(__doc__)
    parser.add_argument("filename", help="output filename for the diagram")
    main(parser.parse_args())
