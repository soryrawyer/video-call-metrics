# daily video call metrics

an application that:

1. starts new video calls
2. generates a unique link for each call, such that others can use the link to join the call
3. has a dashboard that shows all video calls that happened
4. for each participant for each call, graphs:

   a. videoRecvBitsPerSecond

   b. videoSendBitsPerSecond

   c. videoRecvPacketLoss

   d. videoSendPacketLoss

## deliverables

1. a link to the hosted application: https://daily.sawyer.dev/
2. this repository

## approach

### architecture

![a portrait of the video call service as a young architecture](./img/video-call-metrics.png)

### setting up a development environment

If you have [`pipenv`](https://pipenv.pypa.io/en/latest/) installed, then `make install` should create a new virtual environment and install all dependencies into that environment.

### running the application locally

1. [install `docker-compose`](https://docs.docker.com/compose/install/)
2. from the root of this repository:

   a. start the application by running `make up`

   b. stop the application by running `make down`

**Note:** to ensure the application is no longer running, you should no longer see a record with `video-call-metrics_web_1` in the `name` column when you run `docker-compose ps`
