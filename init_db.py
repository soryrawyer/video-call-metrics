"""
initialize our sqlite db
"""

import os

import argparse
import sqlite3


def main(args):
    db = sqlite3.connect(os.environ.get("DB_PATH", "stats.db"))
    if args.clean:
        cur = db.cursor()
        cur.execute("drop table metrics")
        cur.execute("drop table raw_stats")
        db.commit()
        cur.close()

    cur = db.cursor()
    cur.execute(
        """
    create table if not exists metrics (
        user_id text,
        user_name text,
        room_name text,
        session_id text,
        timestamp integer,
        type text,
        value real
    )
    """
    )

    cur.execute(
        """
    create table if not exists raw_stats (payload blob)
    """
    )
    db.commit()
    cur.close()
    db.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(__doc__)
    parser.add_argument("--clean", action="store_true")
    main(parser.parse_args())
