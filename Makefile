.PHONY: install test build up down docker docker-shell

install:
	pipenv install

# TODO: run this in the pipenv virtual environment
test:
	python3 -m pytest

build:
	docker-compose build

up: build
	docker-compose up -d

down:
	docker-compose down

docker:
	docker build . -t video-call-metrics:test

docker-shell:
	docker run -it --entrypoint /bin/sh video-call-metrics:test
