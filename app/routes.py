"""
create a Flask blueprint for our routes
"""

import os
import sqlite3

from datetime import datetime
from typing import Optional

import requests

from flask import Blueprint, json, request
from flask.templating import render_template

from . import storage
from .config import DB_PATH

routes = Blueprint("routes", __name__)


@routes.route("/ping")
def ping():
    """
    this is just for testing and healthchecks
    """
    return json.jsonify({"pong": datetime.now()})


@routes.route("/")
def index():
    """
    return just a basic welcome page with links to the two pages we offer
    """
    return render_template("index.html")


@routes.route("/call/")
@routes.route("/call/<string:room_name>")
def start_call_page(room_name: Optional[str] = None):
    # TODO: pull the base URL out into a configurable variable
    room_url = (
        "https://rory.daily.co/{}".format(room_name) if room_name is not None else None
    )
    return render_template("call.html", roomUrl=room_url)


@routes.route("/create_call")
def create_call():
    """
    create a daily call and return the url
    """
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(os.environ.get("DAILY_API_KEY")),
    }
    resp = requests.post("https://api.daily.co/v1/rooms", headers=headers)
    return json.jsonify({"roomUrl": resp.json()["url"]})


@routes.route("/stats/")
def show_stats():
    """
    return a rendered "stats" template that shows all calls for which we have
    data available
    """
    with sqlite3.connect(DB_PATH) as db:
        return render_template("stats.html", calls=storage.get_calls(db))


@routes.route("/stats/<string:session_id>")
def get_stats_for_session(session_id):
    with sqlite3.connect(DB_PATH) as db:
        return json.jsonify(storage.get_stats_for_call(db, session_id).to_json())


@routes.route("/stats/", methods=["POST"])
def post_stats():
    """
    accept a stats JSON payload and store the values
    """
    data = request.json
    print(data)
    metrics = storage.stats_to_metrics(data)
    # we're connecting to sqlite3 here because SQLite's default behavior is
    # to require all insert operations occur within the same thread!
    with sqlite3.connect(DB_PATH) as db:
        storage.log_stats(db, metrics)
    return json.jsonify({"success": True}), 200, {"ContentType": "application/json"}
