"""
A builder for the Flask application. This should collate any app blueprints,
configuration variables, etc that the application needs
"""


from flask import Flask

from .routes import routes


def build():
    """
    build an instance of our Flask app
    """
    app = Flask(__name__)
    app.register_blueprint(routes)
    return app
