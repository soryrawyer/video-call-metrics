// This is the "library code" for the call page.
// There is some scripting that relies on backend templating,
// but none of this code does.

function startCallAndJoin() {
    fetch("/create_call").then(async resp => {
        let json = await resp.json();
        return json.roomUrl;
    }).then(roomUrl => {
        joinCall(roomUrl);
    });
}

/**
 * Join a Daily video call
 * Get the name of the room we need to join, create a new callFrame, and join
 * the call.
 * Then, set an event handler for when we have actually joined a meeting
 * @param {string} roomUrl the URL of the Daily room we are joining
 */
function joinCall(roomUrl) {
    roomName = new URL(roomUrl).pathname.split("/").pop();
    callFrame = window.DailyIframe.createFrame();
    callFrame.join({ url: roomUrl });
    hideCallButton();
    showUrl(roomUrl);
    callFrame.on("joined-meeting", afterJoin);
}

/**
 * get the meeting's session ID and then schedule polling for stats
 */
function afterJoin() {
    callFrame.getMeetingSession().then(resp => {
        sessionId = resp.meetingSession.id;
        setInterval(sendNetworkStats, 2000);
    });
}

/**
 * We don't want to let users create a new call while they're currently in a
 * call, so hide the call button
 */
function hideCallButton() {
    const button = document.getElementById("start-call");
    button.style.visibility = "hidden";
}

/**
 * Show the call URL for users to share
 * @param {string} roomUrl the URL for _our application_, not the Daily call URL
 */
function showUrl(roomUrl) {
    const urlAnchor = document.getElementById("call-url");
    const callUrl = buildUrl(roomUrl);
    urlAnchor.innerText = callUrl;
    urlAnchor.href = callUrl;

    const shareLinkText = document.getElementById("call-share-text")
    shareLinkText.style.visibility = "visible";
}

/**
 * Convert the call URL to use our application's domain, not daily.co
 * @param {string} roomUrl the Daily video call URL 
 * @return {string} the link to our application's video call
 */
function buildUrl(roomUrl) {
    if (normalizedPathComponents().length > 1) return window.location.href;
    let url = new URL(roomUrl);
    return `${window.location.href}${url.pathname.substring(1)}`;
}

/**
 * @returns {string} any non-empty parts of our window's pathname
 */
function normalizedPathComponents() {
    return window.location.pathname.split("/").filter(i => i != "");
}

/**
 * Call Daily's getNetworkStats method and send the data to our server
 * 
 * Even though we're only displaying certain metrics in our /stats page, 
 * we're sending the entire stats blob for now so we can perform our own
 * analyses
 */
async function sendNetworkStats() {
    let stats = await callFrame.getNetworkStats();
    const local = callFrame.participants().local;
    stats.roomInfo = {
        userId: local.user_id,
        userName: local.user_name,
        roomName: roomName,
        sessionId: sessionId
    };
    fetch("/stats/", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(stats)
    });
}