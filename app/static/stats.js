const select = document.getElementById("call-select");
select.onchange = handleSelectChange;


// We're not selecting the "select a call..." option, so let's
// show the statistics for the selected call
if (select.selectedOptions[0].value !== "skip") {
    getCallStats(select.selectedOptions[0].value);
}


function handleSelectChange(event) {
    getCallStats(event.target.value);
}


// call our stats endpoint for the selected call
function getCallStats(session_id) {
    fetch(`/stats/${session_id}`)
        .then(resp => resp.json())
        .then(resp => {
            console.log(resp);
            generatePlots(resp);
        })
        .catch(err => {
            console.log(`error: ${err}`);
        });
}


// create graphs for each metric we're interested in
function generatePlots(stats) {
    createPlot("videoSendBitsPerSecond", stats.participant_stats);
    createPlot("videoRecvBitsPerSecond", stats.participant_stats);
    createPlot("videoSendPacketLoss", stats.participant_stats);
    createPlot("videoRecvPacketLoss", stats.participant_stats);
}


function createPlot(id, participants) {
    const data = participants.map(part => {
        return {
            x: part.timestamps.map(formatTimestamp),
            y: part[id],
            name: part.user_name
        }
    });

    const el = document.getElementById(id);
    Plotly.newPlot(el, data, {
        margin: { t: 0 }
    });
}


function formatTimestamp(ts) {
    const date = new Date(ts);
    const month = ("0" + date.getMonth()).substr(-2);
    const day = ("0" + date.getDate()).substr(-2);
    const hour = ("0" + date.getHours()).substr(-2);
    const minutes = ("0" + date.getMinutes()).substr(-2);
    const seconds = ("0" + date.getSeconds()).substr(-2);
    return `${date.getFullYear()}-${month}-${day}T${hour}:${minutes}:${seconds}`;
}

