"""
storage.py - a module for modeling and storing call statistics
"""

import sqlite3

from dataclasses import dataclass
from typing import Optional


@dataclass
class Metric:
    user_id: str
    user_name: str
    room_name: str
    session_id: str
    timestamp: int
    metric_type: str
    value: float

    def to_json(self) -> dict:
        return {
            "user_id": self.user_id,
            "user_name": self.user_name,
            "room_name": self.room_name,
            "session_id": self.session_id,
            "timestamp": self.timestamp,
            self.metric_type: self.value,
        }

    def to_sqlite_tuple(self) -> tuple:
        """
        return a tuple of all our stuff!
        this will hopefully make it easier to read the code
        that writes these to SQLite!
        """
        return (
            self.user_id,
            self.user_name,
            self.room_name,
            self.session_id,
            self.timestamp,
            self.metric_type,
            self.value,
        )


@dataclass
class ParticipantCallStats:
    """
    Call statistics for one participant
    """

    user_id: str
    user_name: str
    timestamps: list[int]
    videoRecvBitsPerSecond: list[float]
    videoSendBitsPerSecond: list[float]
    videoRecvPacketLoss: list[float]
    videoSendPacketLoss: list[float]

    def to_json(self) -> dict:
        return {
            "user_id": self.user_id,
            "user_name": self.user_name,
            "timestamps": self.timestamps,
            "videoRecvBitsPerSecond": self.videoRecvBitsPerSecond,
            "videoSendBitsPerSecond": self.videoSendBitsPerSecond,
            "videoRecvPacketLoss": self.videoRecvPacketLoss,
            "videoSendPacketLoss": self.videoSendPacketLoss,
        }


@dataclass
class CallStatRespose:
    """
    a dataclass for representing the object we'll send to the client with
    stats for a specific call
    """

    session_id: str  # AKA "call id"
    participant_stats: list[ParticipantCallStats]

    def to_json(self) -> dict:
        return {
            "session_id": self.session_id,
            "participant_stats": [ps.to_json() for ps in self.participant_stats],
        }


def stats_to_metrics(blob: dict) -> list[Metric]:
    """
    translate the data POSTed to the /stats endpoint to a list of Metrics
    """
    stats = blob["stats"]["latest"]
    result = []
    room_meta = blob["roomInfo"]
    for key, value in stats.items():
        if key == "timestamp":
            continue

        if value is None or value < 0:
            # so far, we don't have any metrics that can be negative
            continue

        # create metric
        metric = Metric(
            room_meta["userId"],
            room_meta["userName"],
            room_meta["roomName"],
            room_meta["sessionId"],
            stats["timestamp"],
            key,
            value,
        )
        result.append(metric)
    return result


def log_stats(db, metrics: list[Metric]):
    """
    insert things into SQLite
    """
    cur = db.cursor()
    entries = [m.to_sqlite_tuple() for m in metrics]
    cur.executemany(
        "insert into metrics values (?, ?, ?, ?, ?, ?, ?)",
        entries,
    )
    db.commit()
    cur.close()


def get_calls(db):
    """
    return a list of unique video calls (session_ids)
    """
    cur = db.cursor()
    for row in cur.execute("select distinct session_id from metrics"):
        yield row[0]


def get_stats_for_call(db, session_id: str) -> CallStatRespose:
    """
    collect stats for each participant in the call and collect them
    into a single CallStatResponse object
    """
    db.row_factory = sqlite3.Row
    cur = db.cursor()
    sql = """
    select user_id, user_name, timestamp, type, value
    from metrics
    where session_id = :session_id
    order by user_id, timestamp asc
    """
    resp = CallStatRespose(session_id, [])
    participant: Optional[ParticipantCallStats] = None
    seen_timestamps: set[int] = set()
    for row in cur.execute(sql, {"session_id": session_id}):
        if participant is None:
            # set participant call stats with initial values
            participant = ParticipantCallStats(
                row["user_id"], row["user_name"], [], [], [], [], []
            )
        elif row["user_id"] != participant.user_id:
            # new participant! add the current participant to the
            # response object, create a new participant object,
            # and reset the timestamp set
            resp.participant_stats.append(participant)
            seen_timestamps = set()
            participant = ParticipantCallStats(
                row["user_id"], row["user_name"], [], [], [], [], []
            )

        if row["timestamp"] not in seen_timestamps:
            participant.timestamps.append(row["timestamp"])
            seen_timestamps.add(row["timestamp"])

        if row["type"] == "videoRecvBitsPerSecond":
            participant.videoRecvBitsPerSecond.append(row["value"])
        elif row["type"] == "videoSendBitsPerSecond":
            participant.videoSendBitsPerSecond.append(row["value"])
        elif row["type"] == "videoRecvPacketLoss":
            participant.videoRecvPacketLoss.append(row["value"])
        elif row["type"] == "videoSendPacketLoss":
            participant.videoSendPacketLoss.append(row["value"])

    if participant is not None:
        resp.participant_stats.append(participant)
    return resp
