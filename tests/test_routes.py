"""
Flask application tests
"""

import json

import pytest

from app import main


@pytest.fixture
def client():
    app = main.build()
    with app.test_client() as client:
        yield client


def test_pong(client):
    """
    test our application by playing a friendly game of ping pong
    """
    assert "pong" in json.loads(client.get("/ping").data)
