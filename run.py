"""
a small entrypoint into our Flask application
"""

from app import main

app = main.build()
